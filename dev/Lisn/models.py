# -*- coding: utf-8 -*-
"""Modèle de données du SI LISN.

Utilisé par Django pour construire les requêtes ainsi que certains formulaires, contrôles de validité…
"""
from django.db import models
from datetime import datetime

# TODO: Normalisation des identificateurs:
# Pour les classes, CamelCase.
# Noms en anglais, lowerCamelCase pour les champs et tables, sauf tables de liaison au
# format nomPremiereTable_nomDeuxiemeTable.
# Pas d'accent dans les identificateurs.

# TODO: Documenter les tables et les attributs.
# https://docs.djangoproject.com/en/dev/internals/contributing/writing-documentation/

# TODO: formater le code en suivant à peu près le PEP8.
# http://sametmax.com/le-pep8-en-resume/
# https://www.python.org/dev/peps/pep-0008/
class backoffice(models.Model):
    """Classe de test à supprimer.
    """
    message = models.CharField(max_length=100, null=True)

    class Meta:
        db_table = "backOffice"


class FunctionMember(models.Model):
    """
    """
    upsaclayCode = models.CharField(max_length=32, null=False)
    fonction_fr = models.CharField(max_length=255, null=False)
    fonction_en = models.CharField(max_length=255, null=True, blank=True)
    cnrsCode = models.CharField(max_length=45, null=False)
    tutelle = models.CharField(max_length=20, null=False)
    
    class Meta:
        db_table = "functionMember"


class Organization(models.Model):
    """
    """
    organization = models.CharField(max_length=100, null=True)

    class Meta:
        db_table = "organization"


class licence(models.Model):
    """
    """
    acronym = models.CharField(max_length=100, null=True)
    title = models.CharField(max_length=100, null=True)
    url = models.CharField(max_length=100, null=True)

    class Meta:
        db_table = "licence"


class PhdHdr(models.Model):
    """
    """
    message = models.CharField(max_length=100, null=True)

    class Meta:
        db_table = "PhdHdr"

    
class Member(models.Model):
    """
    """
    # id= models.AutoField(primary_key=True)
    lastname = models.CharField(max_length=100, null=True)
    firstname = models.CharField(max_length=100)
    gender = models.CharField(max_length=5, blank=True)
    email = models.EmailField(max_length=100, blank=True)
    phoneNumber = models.CharField(max_length=100, blank=True)
    faxNumber = models.CharField(max_length=100, blank=True)
    emailPSaclay = models.EmailField(max_length=100, blank=True)
    statusId = models.CharField(max_length=1, blank=True)
    # organization_id = models.CharField(max_length=10)
    birthDate = models.CharField(max_length=100, null=True, blank=True)
    entryDate = models.CharField(max_length=100, null=True, blank=True)
    departureDate = models.CharField(max_length=100, null=True, blank=True)
    permanentDate = models.CharField(max_length=100, null=True, blank=True)
    NightAccessDate = models.CharField(max_length=100, null=True, blank=True)
    badgeNumber = models.CharField(max_length=32, null=True, blank=True)
    date_ouv_cpt = models.CharField(max_length=32, null=True, blank=True)
    DATE_FERM_CPT = models.CharField(max_length=32, null=True, blank=True)
    nationalityId = models.CharField(max_length=80, null=True, blank=True)
    # TODO: renommer: ce n'est pas un choix de genre.
    GENDER_CHOICES = (('ingénieur', 'ingénieur'),
                      ('doctorant', 'doctorant'),
                      ('stagiaire', 'stagiaire'))
    fonctionId = models.CharField(max_length=100, choices=GENDER_CHOICES, blank=True)
    # TODO: renommer: ce n'est pas un choix de genre.
    GENDER_CHOICES1 = (('CNRS', 'CNRS'),
                       ('Centrale supélec', 'Centrale supélec'),
                       ('CEA', 'CEA'))
    login = models.CharField(max_length=40, blank=True)
    partialTime = models.CharField(max_length=50, null=True, blank=True)
    presenceRate = models.CharField(max_length=50, null=True, blank=True)
    # organisme = models.CharField(max_length=100, choices=GENDER_CHOICES1)
    # GENDER_CHOICES2 = (('Staff','Staff'), ('VALS','VALS'), ('DEV','DEV'))
    # equipe = models.CharField(max_length=100, choices=GENDER_CHOICES2)
    # TODO: renommer: ce n'est pas un choix de genre.
    GENDER_CHOICES3 = (('ok_tech', 'ok_tech'),
                       ('ok', 'ok'))
    validation = models.CharField(max_length=100, choices=GENDER_CHOICES3, null=True, blank=True)
    personalPage = models.CharField(max_length=50, null=True, blank=True)

    class Meta:
        db_table = "member"


class News(models.Model):
    """Nouvelles à afficher sur le site web.

    Voir: Ça pourrait être directement dans le site web vitrine publique pltôt que dans le SI.
    """
    # id= models.CharField(max_length=100,primary_key=True)
    titre_fr = models.CharField(max_length=255, null=True)
    titre_en = models.CharField(max_length=255, null=True)
    url = models.CharField(max_length=255, null=True)
    date_deb = models.CharField(max_length=255, null=True)
    date_exp = models.CharField(max_length=255, null=True)
    bref_fr = models.TextField(blank=True)
    bref_en = models.TextField(blank=True)

    class Meta:
        db_table = "news"
    
          
class Funding(models.Model):
    """Source de financement pour des thèses.
    """
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=20, null=True)
    label = models.CharField(max_length=100)
    
    class Meta:
        db_table = "financement_these"
    
    
class Patent(models.Model):
    """
    """
    #  id= models.AutoField(primary_key=True)
    NOM = models.CharField(max_length=20, null=True)
    # TITRE_FR= models.CharField(max_length=100)
    
    class Meta:
        db_table = "patent"


class demande_encours(models.Model):
    """
    """
    nom = models.CharField(max_length=100, null=True)
    prenom = models.CharField(max_length=100, null=True)
    emailperso = models.EmailField(max_length=100) 
    arrivee = models.CharField(max_length=100, null=True)
    depart = models.CharField(max_length=100, null=True)
    retour = models.CharField(max_length=5, null=True)
    login = models.CharField(max_length=10, null=True)
    statut = models.CharField(max_length=20, null=True)
    date_init = models.CharField(max_length=20, null=True)
    groupe = models.CharField(max_length=20, null=True)
    encadrant = models.CharField(max_length=70, null=True)
    bureau = models.CharField(max_length=70, null=True)
    tel = models.CharField(max_length=70, null=True)
    ordi = models.CharField(max_length=70, null=True)
    thema = models.CharField(max_length=70, null=True)
    statut = models.CharField(max_length=70, null=True)
    paie = models.CharField(max_length=70, null=True)
    detailstatut = models.CharField(max_length=70, null=True, blank=True)
    detailpaiecnrs = models.CharField(max_length=70, null=True, blank=True)
    detailcddit = models.CharField(max_length=70, null=True, blank=True)
    detailcddch = models.CharField(max_length=70, null=True, blank=True)
    detailpaieautre = models.CharField(max_length=70, null=True, blank=True)

    class Meta:
        db_table = "demande_encours"


class respadministrative(models.Model):    
    """
    """
    typeRespFr = models.CharField(max_length=255, null=False)
    typeRespEn = models.CharField(max_length=255, null=True, blank=True)
    nivResp = models.IntegerField(max_length=6, null=True, blank=True)
    url = models.CharField(max_length=255, null=True, blank=True)
    commentaires = models.CharField(max_length=255, null=True, blank=True)
    
    class Meta:
        db_table = "respadministrative"


            
class member_respadministrative  (models.Model):    
    """
    """
    resp_id= models.IntegerField(null=False)
    member_id= models.IntegerField(null=False)
    class Meta:
        db_table ="member_respadministrative"
     
            
            
            
            
            
            
            
class contract (models.Model):    
    """
    """
    id= models.AutoField(primary_key=True)
    administratorIr= models.IntegerField(null=False)
    acronym= models.CharField(max_length=255,null=True, blank=True)
    titleFr= models.CharField(max_length=255,null=True, blank=True)
    titleEn= models.CharField(max_length=255,null=True, blank=True)
    TYPE_CT= models.CharField(max_length=40,null=True, blank=True)
    startDate= models.CharField(max_length=40,null=True, blank=True)
    endDate= models.CharField(max_length=40,null=True, blank=True)
    descriptionFr= models.CharField(max_length=250,null=True, blank=True)
    descriptionEn= models.CharField(max_length=255,null=True, blank=True)
    NOM_APPEL_CT_FR= models.CharField(max_length=255,null=True, blank=True)
    NOM_APPEL_CT_EN= models.CharField(max_length=255,null=True, blank=True)
    url= models.CharField(max_length=255,null=True, blank=True)
    COMMENTAIRES= models.CharField(max_length=255,null=True, blank=True)
   
    
    class Meta:
        db_table ="contract"       
    
class member_contract   (models.Model):    
    """
    """
    member_id= models.IntegerField(null=False)
    contract_id= models.IntegerField(null=True)


    class Meta:
        db_table ="member_contract"  
        
        
        
class member_carrier   (models.Model):    
    """
    """
    member_id= models.IntegerField(null=False)
    carrier_id= models.IntegerField(null=True)


    class Meta:
        db_table ="member_carrier"       
    
    
    
class phdhdr  (models.Model):    
    """
    """
    titleFr= models.CharField(max_length=10,null=False)
    titleEn= models.CharField(max_length=255,null=True, blank=True)
    url= models.CharField(max_length=255,null=True, blank=True)
    abstractFr= models.CharField(max_length=255,null=True, blank=True)
    abstractEn= models.CharField(max_length=40,null=True, blank=True)
    phpDirector= models.CharField(max_length=40,null=True, blank=True)
    phdSupervisorId= models.CharField(max_length=40,null=True, blank=True)
    startDate= models.CharField(max_length=250,null=True, blank=True)
    defenseDate= models.CharField(max_length=255,null=True, blank=True)
    type= models.CharField(max_length=255,null=True, blank=True)
    status= models.CharField(max_length=255,null=True, blank=True)
    comments= models.CharField(max_length=255,null=True, blank=True)
    financementId = models.IntegerField(null=False)
    member_id = models.IntegerField(null=False)
    majorResult_id= models.IntegerField(null=False)
    
    class Meta:
        db_table ="phdhdr"    
    
    
    
    
class carriers (models.Model):    
    """
    """
    description= models.CharField(max_length=255,null=False)
    
    class Meta:
        db_table ="carriers"    
    
    
    
    
class collaboration   (models.Model):    
    """
    """
    typeFr= models.CharField(max_length=255,null=True)
    typeEn= models.CharField(max_length=255,null=True)
    descriptionEn= models.CharField(max_length=255,null=True)
    descriptionFr= models.CharField(max_length=255,null=True)
    startDate= models.CharField(max_length=255,null=True)
    endDate= models.CharField(max_length=255,null=True)
    URL_COLLAB= models.CharField(max_length=255,null=True)
    publications= models.CharField(max_length=255,null=True)
    comments= models.CharField(max_length=255,null=True)
    titleFr= models.CharField(max_length=255,null=True )
    titleEn= models.CharField(max_length=255,null=True)
    results= models.CharField(max_length=255,null=True)
    TYPE_COLLAB_FR= models.CharField(max_length=255,null=False)
    ENTITE_COLLAB= models.CharField(max_length=255,null=False)

    class Meta:
        db_table ="collaboration" 


class member_collaboration (models.Model):    
    """
    """
    collaboration_id= models.IntegerField(null=False)
    member_id= models.IntegerField(null=False)

    class Meta:
        db_table ="member_collaboration"      
        
        
    
class member_topic   (models.Model):    
    """
    """
    topic_id= models.IntegerField(null=False)
    member_id= models.IntegerField(null=True)

    class Meta:
        db_table ="member_topic"     
    
class topic   (models.Model):    
    """
    """
    topicNameFr= models.CharField(max_length=255,null=False)
    topicNameEn= models.CharField(max_length=255,null=True)


    class Meta:
        db_table ="topic"     


class member_project   (models.Model):    
    """
    """
    project_id= models.IntegerField(null=False)
    member_id= models.IntegerField(null=False)

    class Meta:
        db_table ="member_project"     


class project   (models.Model):    
    """
    """
    titleFr= models.CharField(max_length=255,null=False)
    titleEn= models.CharField(max_length=255,null=True , blank=True )
    url= models.CharField(max_length=255,null=True, blank=True)
    typeFr= models.CharField(max_length=255,null=True, blank=True)
    typeEn= models.CharField(max_length=255,null=True, blank=True)
    publications= models.CharField(max_length=255,null=True, blank=True)
    descriptionEn= models.CharField(max_length=255,null=True, blank=True)
    descriptionFr= models.CharField(max_length=255,null=True, blank=True)
    comments= models.CharField(max_length=255,null=True, blank=True)
    externalmembers= models.CharField(max_length=255,null=True, blank=True)
    
    class Meta:
        db_table ="project"     


class majorresult   (models.Model):    
    """
    """
    titleFr= models.CharField(max_length=255,null=False)
    titleEn= models.CharField(max_length=255,null=True , blank=True )
    url= models.CharField(max_length=255,null=True, blank=True)
    typeFr= models.CharField(max_length=255,null=True, blank=True)
    typeEn= models.CharField(max_length=255,null=True, blank=True)
    abstractFr= models.CharField(max_length=255,null=True, blank=True)
    descriptionEn= models.CharField(max_length=255,null=True, blank=True)
    descriptionFr= models.CharField(max_length=255,null=True, blank=True)
    abstractEn= models.CharField(max_length=255,null=True, blank=True)
    resultDate= models.CharField(max_length=255,null=True, blank=True)
    image= models.CharField(max_length=11,null=True, blank=True)
    comments= models.CharField(max_length=255,null=True, blank=True)
    
    class Meta:
        db_table ="majorresult"     
               
                
class member_majorresult  (models.Model):    
 
    result_id= models.IntegerField(null=False)
    member_id= models.IntegerField(null=True)


    class Meta:
        db_table ="member_majorresult"          
        
        
class software   (models.Model):
    """
    """
    name = models.CharField(max_length=255,null=True  )
    titleFr= models.CharField(max_length=255,null=False)
    titleEn= models.CharField(max_length=255,null=True , blank=True )
    depositionDate = models.CharField(max_length=255,null=True, blank=True)
    url= models.CharField(max_length=255,null=True, blank=True)
    typeFr= models.CharField(max_length=255,null=True, blank=True)
    typeEn= models.CharField(max_length=255,null=True, blank=True)
    leaderId= models.IntegerField(null=True, blank=True)
    descriptionEn= models.CharField(max_length=255,null=True, blank=True)
    descriptionFr= models.CharField(max_length=255,null=True, blank=True)
    licence= models.CharField(max_length=100,null=True, blank=True)
    status= models.CharField(max_length=100,null=True, blank=True)
    image= models.CharField(max_length=11,null=True, blank=True)
    comments= models.CharField(max_length=255,null=True, blank=True)
    depositionOrganisationId = models.CharField(max_length=255,null=True, blank=True)
    lastReleaseDate= models.CharField(max_length=255,null=True, blank=True)
    
    class Meta:
        db_table ="software"     
                       
        
class member_software      (models.Model):    
    """
    """
    software_id= models.IntegerField(null=False)
    member_id= models.IntegerField(null=False)

    class Meta:
        db_table ="member_software"     
    
