from django.shortcuts import render
from .forms import Formbackoffice, Formfonction,Formorganisation, Formthèse, Formmembre,Formlicence,Formnews,Formfinancement,Formupdatemembre, Formbrevet, FormNouveauMembre, Formrespadministrative, FormContrat, FormPhd,Formcarriers, Formcollaboration, Formmember_topic , FormTopic, Formmember_respadministrative , Formmember_collaboration, Formmember_project , Formproject , Formmajorresult , Formmember_majorresult , Formmember_carrier , Formmember_contract , Formsoftware , Formmember_software
from .models import backoffice, function_member,organization, thèse,member,news, financements,brevet, respadministrative,contract,phdhdr, carriers, collaboration , topic , member_topic , member_respadministrative, member_collaboration , member_project , project , majorresult, member_majorresult , member_carrier , member_contract , software , member_software
from django.http import HttpResponseRedirect,HttpResponse 
from django.urls import reverse
from django.contrib import messages
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required
import datetime
from django.db import connections
import mysql.connector
from operator import itemgetter
from django_auth_ldap.backend import LDAPBackend
from django.template import loader


def index (request):
    template= loader.get_template('index.html')
    return HttpResponse(template.render())
    


def login1(request):
    username = ""
    password = ""
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        auth = LDAPBackend()
        user = auth.authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("/")
        else:
            error = "Authentication Failed"
            return render(request, "Lisn/login.html")

    return render(request, "Lisn/login.html")


def auth_login(request):

    state = ""
    username = ""
    password = ""
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')

    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')

        print (username, password)

        user = authenticate(username=request.POST.get('username'), password=request.POST.get('password'))
        if user is not None:
            login(request,user)
            state = "Valid account"
            request.session['username'] = username
            return HttpResponseRedirect('http://localhost:8000/backoffice')
        else:
            state = "Inactive account"
    print(state)
    return render(request, 'Lisn/login.html', {'state': state, 'username': username})











def backoffice(request):
    form= Formbackoffice(request.POST or None)
    if form.is_valid():
        form.save()
    username = request.session['username']    
    context= {'form': form , 'username' : username }
    
    print (username)
        
    return render(request, 'Lisn/BackOffice.html', context)




def showformmembre(request):
    form= Formmembre(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
  
        

            return HttpResponseRedirect('http://localhost:8000/membre')
    context= {'form': form }
    return render(request, 'Lisn/membre.html', context)




def fonction_member(request):
    form= Formfonction(request.POST or None)
    
    if form.is_valid():
        form.save()
      
    context= {'form': form }
        
        
    return render(request, 'Lisn/fonction.html', context)
   


def liste_fonctions(request):
    resultsdisplay= function_member.objects.all()
    username = request.session['username']
    resultsdisplay1= member.objects.get(lastname = username)
    print (resultsdisplay1.id)
    

    return render(request, 'Lisn/liste_fonctions.html',{'function_member' :resultsdisplay , 'username' : username } )

def editer_fonction(request,pk):
    resultsdisplay= FunctionMember.objects.get(id= pk)
    return render(request, 'Lisn/edit_function.html',{'FunctionMember' :resultsdisplay } )

def modifier_fonction(request,pk):
    resultsdisplay= FunctionMember.objects.get(id=pk)
    form= Formfonction(request.POST, instance=resultsdisplay)
    if form.is_valid():
        form.save()
        messages.success(request,"ok")
        return render(request, 'Lisn/edit_function.html',{'FunctionMember' :resultsdisplay } )



def liste_membres(request):
    resultsdisplay= Member.objects.all()
    return render(request, 'Lisn/liste_membres.html',{'Member' :resultsdisplay } )



def liste_membres_actuels(request):
    resultsdisplay= Member.objects.filter (departureDate = 2010)
    print(resultsdisplay)
    return render(request, 'Lisn/liste_membres_actuels.html',{'Member' :resultsdisplay } )




def editer_membre(request,pk):
    resultsdisplay= member.objects.get(id= pk)
    resultsdisplay1= respadministrative.objects.all()
    resultsdisplay2= contract.objects.all()
    resultsdisplay3= phdhdr.objects.all()
    resultsdisplay4= carriers.objects.all()
    resultsdisplay5= topic.objects.all()
    resultsdisplay6 = collaboration.objects.all()
    resultsdisplay7 = project.objects.all()
    resultsdisplay8 = majorresult.objects.all()
    resultsdisplay9 = software.objects.all()
    form= Formmember_topic(request.POST or None)
    form1=Formmember_respadministrative(request.POST or None)
    form2 = Formmember_collaboration (request.POST or None)
    form3 = Formmember_project  (request.POST or None)
    form4 = Formmember_majorresult  (request.POST or None)
    form5 = Formmember_carrier  (request.POST or None)
    form6 = Formmember_contract  (request.POST or None)
    form7 = Formmember_software  (request.POST or None)
    username = request.session['username']  
    
    print (username)
    if request.method == "POST":
        if form.is_valid() :
            form.save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        elif form1.is_valid():    
            form1.save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        elif form2.is_valid():    
            form2.save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        elif form3.is_valid():    
            form3.save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        elif form4.is_valid():    
            form4.save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        elif form5.is_valid():    
            form5.save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        elif form6.is_valid():    
            form6.save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        elif form7.is_valid():    
            form7.save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    return render(request, 'Lisn/edit_membre.html',{'member' :resultsdisplay , 'respadministrative': resultsdisplay1, "contract": resultsdisplay2 , "phdhdr" :resultsdisplay3 , "carriers" :resultsdisplay4 , "topic" : resultsdisplay5 , "collaboration" :resultsdisplay6  , "project" :resultsdisplay7, "majorresult": resultsdisplay8 , "software": resultsdisplay9 , "username" : username} )

def modifier_membre(request,pk):
    resultsdisplay= Member.objects.get(id=pk)
    form= Formupdatemembre(request.POST, instance=resultsdisplay)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    messages.success(request,"ok")
    return render(request, 'Lisn/edit_membre.html',{'member' :resultsdisplay } )
    
    
def editer_financements(request,pk):
    resultsdisplay= Funding.objects.get(id= pk)
    return render(request, 'Lisn/edit_financements.html',{'Funding' :resultsdisplay } )

def modifier_financements(request,pk):
    resultsdisplay= Funding.objects.get(id=pk)
    form= Formfinancement(request.POST, instance=resultsdisplay)
    if form.is_valid():
        form.save()
        messages.success(request,"ok")
        return render(request, 'Lisn/edit_financements.html',{'Funding' :resultsdisplay } )
        
    
    
    
    
    
    
    
    
    
    


def organisation(request):
    form= Formorganisation(request.POST or None)
    if form.is_valid():
        form.save()
      
    context= {'form': form }
        
        
    return render(request, 'Lisn/organization.html', context)


def licences(request):
    form= Formlicence(request.POST or None)
    if form.is_valid():
        form.save()
      
    context= {'form': form }
        
        
    return render(request, 'Lisn/licence.html', context)






def thèse(request):
    form= Formthèse(request.POST or None)
    if form.is_valid():
        form.save()
      
    context= {'form': form }
        
        
    return render(request, 'Lisn/PhdHdr.html', context)


def liste_news(request):
    resultsdisplay= News.objects.filter(date_deb__isnull=False)
    return render(request, 'Lisn/liste_news.html',{'News' :resultsdisplay } )



def liste_admResp(request):   
    resultsdisplay= respadministrative.objects.all()
    return render(request, 'Lisn/edit_membre.html',{'respadministrative' :resultsdisplay } )






def delete_news(request,pk):
    delRow = News.objects.get(id= pk)
    delRow.delete()
    resultsdisplay= News.objects.filter(date_deb__isnull=False)
    return render(request, 'Lisn/liste_news.html',{'News' :resultsdisplay } )

def delete_financement(request,pk):
    delRow = Funding.objects.get(id= pk)
    delRow.delete()
    resultsdisplay= Funding.objects.all()
    return render(request, 'Lisn/liste_financements.html',{'Funding' :resultsdisplay } )


def ajout_news(request):
    form= Formnews(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
  
        

            return HttpResponseRedirect('http://localhost:8000/ajout_news')
    context= {'form': form }
    return render(request, 'Lisn/ajout_news.html', context)





def adminResp (request):
    form= Formrespadministrative(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
  
        

            return HttpResponseRedirect('http://localhost:8000/adminResp')
    context= {'form': form }
    return render(request, 'Lisn/adm_resp.html', context)




def ajout_contrat(request):
    form= FormContrat(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
  
        

            return HttpResponseRedirect('http://localhost:8000/ajout_contrat')
    context= {'form': form }
    return render(request, 'Lisn/contrat.html', context)



def ajout_phd(request):
    resultsdisplay= member.objects.all()

    form= FormPhd(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
  
        

            return HttpResponseRedirect('/ajout_phd')
    context= {'form': form ,'member' : resultsdisplay}
    return render(request, 'Lisn/TheseHabilitation.html', context)









def liste_financement(request):
    resultsdisplay= Funding.objects.all()
    return render(request, 'Lisn/liste_financements.html',{'Funding' :resultsdisplay } )


def ajout_financement(request):
    form= Formfinancement(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
  
        

            return HttpResponseRedirect('http://localhost:8000/financements_thèses')
    context= {'form': form }
    return render(request, 'Lisn/financements_thèses.html', context)


def liste_brevets(request):
    allbrevets= Patent.objects.all()
    return render(request, 'Lisn/liste_brevets.html',{'Patent' :allbrevets } )




def ajout_brevet(request):
    form= Formbrevet(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
  
        

            return HttpResponseRedirect('http://localhost:8000/ajout_brevets')
    context= {'form': form }
    return render(request, 'Lisn/ajout_brevets.html', context)



def ajout_nv_membre(request):
    form= FormNouveauMembre(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('http://localhost:8000/nouveau_membre')
    context= {'form': form }
    return render(request, 'Lisn/nouveau_membre.html', context)


def ajout_carriere(request):
    allcarriers= carriers.objects.all()
    form= Formcarriers(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('http://localhost:8000/carriere')
    context= {'form': form, 'carriers': allcarriers }
    return render(request, 'Lisn/carriere.html', context)





    

def ajout_member_topic(request):

    resultsdisplay= topic.objects.all()
    form= Formmember_topic(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('http://localhost:8000/membertopic')
    context= {'form': form, 'topic':resultsdisplay }
    return render(request, 'Lisn/membertopic.html', context)
    
def ajout_topic(request):

    form= FormTopic(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('http://localhost:8000/membertopic')
    context= {'form': form }
    return render(request, 'Lisn/membertopic.html', context)
   
def ajout_collaboration(request):

    form= Formcollaboration(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('http://localhost:8000/collaboration')
    context= {'form': form }
    return render(request, 'Lisn/collaboration.html', context)
       
def ajout_projet(request):

    form= Formproject(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('http://localhost:8000/projet')
    context= {'form': form }
    return render(request, 'Lisn/projet.html', context)    
    
def ajout_majorresult(request):

    form= Formmajorresult(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('http://localhost:8000/majorresult')
    context= {'form': form }
    return render(request, 'Lisn/majorresult.html', context)    
        
def ajout_software(request):

    form= Formsoftware(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('http://localhost:8000/ajout_software')
    context= {'form': form }
    return render(request, 'Lisn/ajout_software.html', context)    
            
    


