from django import forms
from .models import backoffice, function_member,organization, thèse, member,licence,news,financements,brevet, demande_encours, demande_encours, respadministrative, contract, phdhdr,carriers,collaboration , topic , member_topic, member_respadministrative, member_collaboration , member_project, project , member_majorresult , majorresult, member_contract ,member_carrier, software , member_software


class Formbackoffice(forms.ModelForm):
    class Meta:
        model= BackOffice
        fields= ["message"]
        
        
class Formfonction(forms.ModelForm):
    class Meta:
        model= FunctionMember
        fields= ["upsaclayCode", "fonction_fr","fonction_en", "cnrsCode","tutelle"]
        
        
        
class Formorganisation(forms.ModelForm):
    class Meta:
        model= Organization
        fields= ["organization"]
        
        
        
class Formlicence(forms.ModelForm):
    class Meta:
        model= licence
        fields= ["acronym", "title", "url"]
        
        
class Formthèse(forms.ModelForm):
    class Meta:
        model= PhdHdr
        fields= ["message"]
        
        
        
class Formmembre(forms.ModelForm):
    class Meta:
        model= Member
        fields= ["lastname","gender", "firstname", "email","fonctionId","phoneNumber","faxNumber","statusId","personalPage","birthDate","badgeNumber","nationalityId","entryDate","login","validation","departureDate","date_ouv_cpt","partialTime","permanentDate","DATE_FERM_CPT","presenceRate","NightAccessDate" ]
        
        
class Formupdatemembre(forms.ModelForm):
    class Meta:
        model= Member
        fields= ["lastname","firstname", "email","phoneNumber", "faxNumber", "personalPage" , "login","birthDate" , "entryDate","departureDate" , "date_ouv_cpt" ,"DATE_FERM_CPT" ,"partialTime" ,"badgeNumber", "nationalityId" , "presenceRate" ,"permanentDate" ,"NightAccessDate" ,"validation"]
        
         
class Formfinancement(forms.ModelForm):
    class Meta:
        model= Funding
        fields= ["name", "label"]
 

    
class Formnews(forms.ModelForm):
    class Meta:
        model= News
        fields= ["titre_fr", "titre_en","url","date_deb","date_exp","bref_fr","bref_en"]
  

class Formbrevet(forms.ModelForm):
    class Meta:
        model= Patent
        fields= ["NOM"]
  

class FormNouveauMembre(forms.ModelForm):
    class Meta:
        model= demande_encours
        fields= ["nom" , "prenom", "emailperso", "arrivee", "depart","retour", "login","statut","date_init","groupe", "encadrant" , "bureau" , "tel", "ordi","thema","statut", "paie" , "detailstatut","detailpaiecnrs","detailpaieautre","detailcddit","detailcddch"]
        
class FormContrat (forms.ModelForm):
    class Meta:
        model= Contract
        fields= ["acronym" , "administratorIr", "titleFr","titleEn","TYPE_CT","startDate","endDate","descriptionFr","descriptionEn","NOM_APPEL_CT_FR", "NOM_APPEL_CT_EN", "url","COMMENTAIRES" ]
                     
        
        
class   Formrespadministrative (forms.ModelForm):
    class Meta:
        model= respadministrative
        fields= ["typeRespFr" , "typeRespEn", "nivResp", "url", "commentaires"]
             
        
class FormPhd (forms.ModelForm):
    class Meta:
        model= phdhdr
        fields= ["titleFr" , "titleEn", "url", "abstractFr", "phpDirector", "phdSupervisorId","startDate", "defenseDate" , "type" , "status" , "comments", "member_id"]
                      
 
class Formcarriers (forms.ModelForm):
    class Meta:
        model= carriers 
        fields= ["description" ]


        
        
class Formcollaboration  (forms.ModelForm):
    class Meta:
        model= collaboration 
        fields= ["typeFr" , "typeEn" , "descriptionEn", "descriptionFr" , "startDate", "endDate", "URL_COLLAB", "publications", "comments", "titleFr", "titleEn", "results" ,"TYPE_COLLAB_FR","ENTITE_COLLAB"]



class FormTopic  (forms.ModelForm):
    class Meta:
        model= topic 
        fields= ["topicNameFr" , "topicNameEn" ]


        
class Formmember_topic  (forms.ModelForm):
    class Meta:
        model= member_topic 
        fields= ["topic_id" , "member_id" ]



class Formmember_respadministrative (forms.ModelForm):
    class Meta:
        model= member_respadministrative 
        fields= ["resp_id" , "member_id" ]

class Formmember_collaboration     (forms.ModelForm):
    class Meta:
        model= member_collaboration

        fields= ["collaboration_id" , "member_id" ]


        
class Formmember_project    (forms.ModelForm):
    class Meta:
        model= member_project

        fields= ["project_id" , "member_id" ]

class Formproject    (forms.ModelForm):
    class Meta:
        model= project

        fields= ["titleFr" , "titleEn", "url" , "typeFr" , "typeEn", "publications" , "descriptionEn" , "descriptionFr" , "comments" , "externalmembers"]


        
        
class Formmember_majorresult   (forms.ModelForm):
    class Meta:
        model=  member_majorresult

        fields= ["result_id" , "member_id" ]
        
        
class Formmember_contract   (forms.ModelForm):
    class Meta:
        model= member_contract

        fields= ["member_id" , "contract_id" ]
        
        
class Formmember_carrier    (forms.ModelForm):
    class Meta:
        model= member_carrier

        fields= ["member_id" , "carrier_id" ]
             
        
        
        

class Formmajorresult    (forms.ModelForm):
    class Meta:
        model= majorresult

        fields= ["titleFr" , "titleEn", "url" , "typeFr" , "typeEn", "abstractFr" , "descriptionEn" , "descriptionFr" , "abstractEn" , "resultDate" ,"image" , "comments"]        
        
        

   
  
class Formsoftware    (forms.ModelForm):
    class Meta:
        model= software

        fields= ["name" , "titleFr" , "titleEn", "url" , "typeFr" , "typeEn", "depositionDate" , "descriptionEn" , "descriptionFr" , "leaderId" , "licence", "status" ,"image" , "comments" , "depositionOrganisationId" , "lastReleaseDate"]        
                
        
class Formmember_software    (forms.ModelForm):
     class Meta:
            model=  member_software

            fields= ["software_id" , "member_id" ]


        
        
        
        
        
        
        
        
        
        
        
        



