
from django.contrib import admin
from django.urls import path
from Lisn import views
from django.contrib.auth import views as auth_views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('BackOffice', views.BackOffice),
    path('fonction', views.fonction_member),
    path('liste_fonctions', views.liste_fonctions),
    path('organization', views.organization),
    path('PhdHdr', views.PhdHdr),
    path('membre/', views.showformmembre),
    path('liste_membres', views.liste_membres),
    path('licences', views.licences),
    path('liste_news', views.liste_news),
    path('Delete/<int:pk>', views.delete_news),
    path('ajout_news', views.ajout_news),
    path('edit/<int:pk>', views.editer_fonction),
    path('update/<int:pk>', views.modifier_fonction),
    path('editMembre/<int:pk>', views.editer_membre),
    path('updateMembre/<int:pk>', views.modifier_membre),
    path('liste_financements', views.liste_financement),
    path('financements_thèses', views.ajout_financement),
    path('DeleteFinancement/<int:pk>', views.delete_financement),
    path('editFinancement/<int:pk>', views.editer_financements),
    path('updateFinancement/<int:pk>', views.modifier_financements),
    path('liste_brevets', views.liste_brevets),
    path('ajout_brevets', views.ajout_brevet),
    path('nouveau_membre', views.ajout_nv_membre),
    path('liste_membres_actuels', views.liste_membres_actuels),
    path('login/', views.auth_login, name='login'),
    path('myapp', views.index),
    path('adminResp', views.adminResp),
    path('ajout_contrat', views.ajout_contrat),
    path('ajout_phd', views.ajout_phd),
    path('carriere', views.ajout_carriere),
    path('membertopic', views.ajout_topic),
    path('collaboration', views.ajout_collaboration),
    path('projet', views.ajout_projet),
    path('majorresult', views.ajout_majorresult),
    path('ajout_software', views.ajout_software),











    



]
