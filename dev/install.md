# Installation de modules nécessaires pour faire tourner le code

 - python3-django
 - python3-pymysql
 - django_python3_ldap
 - django-auth-ldap
 - jsonfield
 - jango_crispy_forms
 
 Sur ubuntu 18.04, les versions ne sont pas assez récentes

J'ai créé un environnement virtuel devlisn

    virtualenv -p python3 devlisn
    . devlisn/bin/activate
    pip3 install --upgrade django
    pip3 install pymysql
    pip3 install django_crispy_forms

   

## LDAP :

> File "/websitelisn/dev/dev/settings.py", line 20, in <module>
>    import ldap
> ModuleNotFoundError: No module named 'ldap'

    pip3 install django_python3_ldap
    pip3 install django_auth_ldap


