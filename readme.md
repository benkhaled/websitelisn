# Informations générales


## Organisation du dépôt : 

> Note: en [terminologie Django](https://docs.djangoproject.com/en/3.1/intro/tutorial01/#creating-the-polls-app) une *app* est une application web qui fournit un certain service. Et un *project* est une collections d'apps configurées et organisées pour construire un site.

### dev

Projet Django

- db.sqlite3
- manage.py

#### dev

App Django

#### Lisn

App Django

#### static

Fichiers statiques (images, css)

#### templates

## Documentation

On utilise autant que possible la documentation directement dans les
sources comme dans tous les modules Python. 
L'outil [sphinx](https://www.sphinx-doc.org/en/master/) permet ensuite
de construire la documentation au format HTML (ou autre), avec ou sans
les API.

[Howto Shpinx+Django](https://www.freecodecamp.org/news/sphinx-for-django-documentation-2454e924b3bc/)

